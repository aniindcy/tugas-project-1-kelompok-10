<?php
session_start();

// Dummy data kursus
$courses = array(
    "Course Title 1" => array(
        "Description" => "Description 1",
        "Materials" => array(
            "Material 1.1",
            "Material 1.2",
            "Material 1.3"
        )
    ),
    "Course Title 2" => array(
        "Description" => "Description 2",
        "Materials" => array(
            "Material 2.1",
            "Material 2.2",
            "Material 2.3"
        )
    ),
    "Course Title 3" => array(
        "Description" => "Description 3",
        "Materials" => array(
            "Material 3.1",
            "Material 3.2",
            "Material 3.3"
        )
    )
);

// Check if user is not logged in, redirect to login page
if (!isset($_SESSION['user'])){
  header('Location: login.html');
  exit();
}

// Mendapatkan judul kursus yang dipilih
if (isset($_GET['title']) && array_key_exists($_GET['title'], $courses)) {
    $selectedCourse = $_GET['title'];
} else {
    header('Location: home.php'); // Redirect jika judul kursus tidak valid
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title><?php echo $selectedCourse; ?> Detail</title>
  
</head>
<body>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="courses.php">Courses</a></li>
                <li><a href="Logout.php">Logout</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <section class="course-detail">
            <h2><?php echo $selectedCourse; ?></h2>
            <p><strong>Description:</strong> <?php echo $courses[$selectedCourse]['Description']; ?></p>
            <h3>Materials:</h3>
            <ul>
                <?php foreach ($courses[$selectedCourse]['Materials'] as $material): ?>
                    <li><?php echo $material; ?></li>
                <?php endforeach; ?>
            </ul>
            <h3>Upload Task:</h3>
            <!-- Form untuk upload file tugas -->
            <form action="upload_task.php" method="post" enctype="multipart/form-data">
                <label for="task_file">Choose File:</label>
                <input type="file" id="task_file" name="task_file" required>
                <input type="submit" value="Upload Task">
            </form>
        </section>
    </main>
    <footer>
        <p>&copy; 2024 EduTechHub. All rights reserved.</p>
    </footer>
</body>
</html>
